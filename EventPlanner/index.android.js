/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import {authenticationReducer} from './src/authentication';
import {eventReducer} from './src/event';
import {Router} from './src/Router'

const rootReducer = combineReducers({authentication: authenticationReducer, event: eventReducer});
const store = createStore(rootReducer, applyMiddleware(thunk, createLogger({colors: {}})));

export default class EventPlanner extends Component {
    render() {
        return (
            <Router store={store}/>
        );
    }
}

AppRegistry.registerComponent('EventPlanner', () => EventPlanner);
