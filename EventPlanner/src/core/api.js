export const apiUrl = 'http://192.168.43.60:8080/api';
export const headers = {'Accept': 'application/json', 'Content-Type': 'application/json'};
export const authenticationHeaders = (token) => ({...headers, 'x-auth-token': `${token}`});
