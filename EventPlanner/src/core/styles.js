import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    activityIndicator: {
        height: 50
    },
    container: {
        flex: 1,
        marginTop: 50,
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'stretch',
        backgroundColor: '#fff',
    },
    titleText: {
        fontSize: 20,
        textAlign: 'center',
        color: '#3b77ba',
        margin: 10,
    },
    detailTitleText: {
        textAlign: 'justify',
        color: '#7e7e7e',
        fontSize: 15,
        marginTop: 2.5,
        marginBottom: 2.5,
    },
    detailText: {
        textAlign: 'justify',
        color: '#000000',
        marginTop: 1.5,
        marginBottom: 1.5
    },
    errorText: {
        textAlign: 'justify',
        color: '#c9343e',
        marginBottom: 5,
    },
    listItem: {
        fontSize: 20,
        textAlign: 'left',
        color: '#7e7e7e',
        margin: 10,
    },
    blueButton: {
        color: '#fff',
        backgroundColor: '#7e7e7e',
        textAlign: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
    },
    networkText: {
        fontSize: 20,
        textAlign: 'justify',
        color: '#c9343e',
        margin:10,
    },
    box: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 100,
        backgroundColor: '#ccc'
    },
});

export default styles;