import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableHighlight} from 'react-native';
import styles from '../core/styles';

export class EventView extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableHighlight onPress={() => this.props.onPress(this.props.event)}>
                <View>
                    <Text style={styles.listItem}>{this.props.event.id}. {this.props.event.name}</Text>
                </View>
            </TouchableHighlight>
        );
    }
}