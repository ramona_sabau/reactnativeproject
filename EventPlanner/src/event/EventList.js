import React, {Component} from 'react';
import {ListView, Text, View, StatusBar, ActivityIndicator, NetInfo} from 'react-native';
import {EventDetail} from './EventDetail';
import {EventView} from './EventView';
import {EventEdit} from './EventEdit';
import {loadEvents, cancelLoadEvents} from './service';
import {registerRightAction, getLogger, issueToText} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('EventList');
const EVENT_LIST_ROUTE = 'event/list';

export class EventList extends Component {
    static get routeName() {
        return EVENT_LIST_ROUTE;
    }

    static get route() {
        return {name: EVENT_LIST_ROUTE, title: 'Event list', rightText: 'New'};
    }

    constructor(props) {
        super(props);
        log('constructor');
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.id !== r2.id});
        this.store = this.props.store;
        const eventState = this.store.getState().event;
        this.state = {isLoading: eventState.isLoading, dataSource: this.ds.cloneWithRows(eventState.items), connectionInformation: ''};
        registerRightAction(this.props.navigator, this.onNewEvent.bind(this));
    }

    render() {
        log('render');
        let message = issueToText(this.state.issue);
        return (
            <View style={styles.container}>
                <Text style={styles.networkText}>Network connection status: {(this.state.connectionInformation)}</Text>
                { this.state.isLoading &&
                <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                }
                {message && <Text style={styles.errorText}>{message}</Text>}
                <ListView
                    dataSource={this.state.dataSource}
                    enableEmptySections={true}
                    renderRow={event => (<EventView event={event} onPress={(event) => this.onEventPress(event)}/>)}/>
            </View>
        );
    }

    onNewEvent() {
        log('onNewEvent');
        this.props.navigator.push({...EventEdit.route});
    }

    onEventPress(event) {
        log('onEventPress');
        this.props.navigator.push({...EventDetail.route, data: event});
    }

    componentDidMount() {
        log('componentDidMount');
        this._isMounted = true;
        const store = this.store;
        this.unsubscribe = store.subscribe(() => {
            log('setState');
            const eventState = store.getState().event;
            this.setState({dataSource: this.ds.cloneWithRows(eventState.items), isLoading: eventState.isLoading});
        });

        NetInfo.addEventListener(
            'change',
            this._handleConnectionInfoChange
        );

        NetInfo.fetch().done(
            (connectionInfo) => {
                const state = this.state;
                this.setState({...state, connectionInformation: connectionInfo});
            }
        );

        store.dispatch(loadEvents());
    }

    componentWillUnmount() {
        log('componentWillUnmount');
        this._isMounted = false;
        this.unsubscribe();
        if (this.state.isLoading) {
            this.store.dispatch(cancelLoadEvents());
        }

        NetInfo.removeEventListener(
            'change',
            this._handleConnectionInfoChange
        );
    }

    _handleConnectionInfoChange = (connectionInfo) => {
        const state = this.state;
        this.setState({...state, connectionInformation: connectionInfo});
    };
}
