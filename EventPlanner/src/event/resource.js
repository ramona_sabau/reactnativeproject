import {getLogger, ResourceError} from '../core/utils';
import {apiUrl, authenticationHeaders} from '../core/api';

const log = getLogger('event/resource');

export const getAll = async(token) => {
    const url = `${apiUrl}/secured/event`;
    log(`GET ${url}`);
    let ok;
    let json = await fetch(url, {method: 'GET', headers: authenticationHeaders(token)})
        .then(res => {
            ok = res.ok;
            return res.json();
        });
    return interpretResult('GET', url, ok, json);
};

export const save = async(token, event) => {
    log(`save`);
    const body = JSON.stringify(event);
    const url = `${apiUrl}/secured/event`;
    const method = event.id ? 'PUT' : 'POST';
    log(`${method} ${url}`);
    let ok;
    let json = await fetch(url, {method, headers: authenticationHeaders(token), body})
        .then(res => {
            ok = res.ok;
            return res.json();
        });
    return interpretResult(method, url, ok, json);
};

export const erase = async(token, id) => {
    log(`erase`);
    const url = `${apiUrl}/secured/event/${id}`;
    const method = 'DELETE';
    log(`${method} ${url}`);
    let ok;
    let json = await fetch(url, {method, headers: authenticationHeaders(token)})
        .then(res => {
            ok = res.ok;
            return res.json();
        });

    return interpretResult(method, url, ok, json);
};

function interpretResult(method, url, ok, json) {
    if (ok) {
        log(`${method} ${url} succeeded`);
        return json;
    } else {
        log(`${method} ${url} failed`);
        let errorMessage = 'Error occurred! ';

        if (json.error) {
            errorMessage += json.error;
        }

        if (json.name) {
            errorMessage += json.name;
        }

        if (json.description) {
            errorMessage += json.description;
        }

        if (json.author) {
            errorMessage += json.author;
        }
        throw new ResourceError(errorMessage, json);
    }
}