import React, {Component} from 'react';
import {Text, View, TextInput, ActivityIndicator, TouchableHighlight} from 'react-native';
import {EventEdit} from './EventEdit';
import {registerRightAction, issueToText, getLogger, ResourceError} from '../core/utils';
import {deleteEvent, cancelDeleteEvent, loadEvents, cancelLoadEvents} from './service';
import styles from '../core/styles';
import {EventList} from "./EventList";

const log = getLogger('EventDetail');
const EVENT_EDIT_ROUTE = 'note/edit';

export class EventDetail extends Component {
    static get routeName() {
        return EVENT_EDIT_ROUTE;
    }

    static get route() {
        return {name: EVENT_EDIT_ROUTE, title: 'Event detail', rightText: 'Edit'};
    }

    constructor(props) {
        log('constructor');
        super(props);
        this.store = this.props.store;
        const nav = this.props.navigator;
        this.navigator = nav;
        const currentRoutes = nav.getCurrentRoutes();
        const currentRoute = currentRoutes[currentRoutes.length - 1];
        if (currentRoute.data) {
            this.state = {event: {...currentRoute.data}};
        } else {
            this.state = {event: {name: '', description: '', author: {id: 0, firstName: '', lastName: '', username: '', email: ''}}};
        }
        registerRightAction(this.props.navigator, this.onNewEvent.bind(this));
    }

    render() {
        log('render');
        const currentState = this.state;
        const state = this.store.getState();
        let message = issueToText(currentState.issue);
        return (
            <View style={styles.container}>
                <Text style={styles.detailTitleText}>Name:</Text>
                <Text style={styles.detailText}>{currentState.event.name}</Text>
                <Text style={styles.detailTitleText}>Description:</Text>
                <Text style={styles.detailText}>{currentState.event.description}</Text>
                <Text style={styles.detailTitleText}>Author:</Text>
                <Text style={styles.detailText}>{currentState.event.author.firstName} {currentState.event.author.lastName}</Text>
                <TouchableHighlight onPress={() => this.onDeleteEvent()}>
                    <View>
                        <Text style={styles.blueButton}>Delete</Text>
                    </View>
                </TouchableHighlight>
                { state.event.isDeleting &&
                <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                }
                {message && <Text style={styles.errorText}>{message}</Text>}
            </View>
        )
    }

    componentDidMount() {
        log('componentDidMount');
        this._isMounted = true;
        const store = this.props.store;
        this.unsubscribe = store.subscribe(() => {
            log('setState');
            const state = this.state;
            const eventState = store.getState().event;
            this.setState({...state, issue: eventState.issue});
        });
    }

    componentWillUnmount() {
        log('componentWillUnmount');
        this._isMounted = false;
        this.unsubscribe();
        let state = this.store.getState();
        if (state.isDeleting) {
            this.store.dispatch(cancelDeleteEvent());
        }
        if (state.isLoading) {
            this.store.dispatch(cancelLoadEvents());
        }
    }

    onNewEvent() {
        log('onNewEvent');
        const state = this.store.getState();
        if (state.authentication.user.id != this.state.event.author.id) {
            log(`onSave cannot be executed the user does not rights`);
            const currentState = this.state;
            this.setState({...currentState, issue: [{error: 'You are not authorized to change this event.'}]});
        } else {
            this.props.navigator.push({...EventEdit.route, data: this.state.event});
        }
    }

    onDeleteEvent() {
        log('onDeleteEvent');
        const state = this.store.getState();
        if (state.authentication.user.id != this.state.event.author.id) {
            log(`onEventDeleted cannot be executed the user does not rights`);
            const currentState = this.state;
            this.setState({...currentState, issue: [{error: 'You are not authorized to delete this event.'}]});
        } else {
            this.store.dispatch(deleteEvent(this.state.event.id)).then(() => {
                log('onEventDeleted');
                if (!this.state.issue) {
                    this.store.dispatch(loadEvents());
                    var routes = this.navigator.state.routeStack;
                    for (var i = routes.length - 1; i >= 0; i--) {
                        if (routes[i].name === EventList.routeName) {
                            var destinationRoute = this.props.navigator.getCurrentRoutes()[i];
                            this.navigator.popToRoute(destinationRoute);
                        }
                    }
                }
            });
        }
    }
}