import React, {Component} from 'react';
import {Text, View, TextInput, ActivityIndicator} from 'react-native';
import {saveEvent, cancelSaveEvent} from './service';
import {EventList} from './EventList';
import {registerRightAction, issueToText, getLogger} from '../core/utils';
import styles from '../core/styles';
import * as Animatable from 'react-native-animatable';

const log = getLogger('EventEdit');
const EVENT_EDIT_ROUTE = 'event/edit';

export class EventEdit extends Component {
    static get routeName() {
        return EVENT_EDIT_ROUTE;
    }

    static get route() {
        return {name: EVENT_EDIT_ROUTE, title: 'Event edit', rightText: 'Save'};
    }

    constructor(props) {
        log('constructor');
        super(props);
        this.store = this.props.store;
        const nav = this.props.navigator;
        this.navigator = nav;
        const currentState = this.store.getState();
        const currentRoutes = nav.getCurrentRoutes();
        const currentRoute = currentRoutes[currentRoutes.length - 1];
        if (currentRoute.data) {
            this.state = {event: {...currentRoute.data}};
        } else {
            this.state = {event: {name: '', description: '', author: currentState.authentication.user}};
        }
        registerRightAction(nav, this.onSave.bind(this));
    }

    render() {
        log('render');
        const currentState = this.state;
        let message = issueToText(currentState.issue);
        const state = this.store.getState();
        return (
            <View style={styles.container}>
                <Animatable.View
                    ref={'pulse'}
                    style={[styles.box, { backgroundColor: '#FFC600' }]}
                    animation={'pulse'}
                    iterationCount={"infinite"}>
                    <Text style={styles.detailTitleText}>Name:</Text>
                </Animatable.View>
                <TextInput value={currentState.event.name} onChangeText={(text) => this.updateEventName(text)}/>
                <Animatable.View
                    ref={'pulse'}
                    style={[styles.box, { backgroundColor: '#FFC600' }]}
                    animation={'pulse'}
                    iterationCount={"infinite"}>
                    <Text style={styles.detailTitleText}>Description:</Text>
                </Animatable.View>
                <TextInput value={currentState.event.description} onChangeText={(text) => this.updateEventDescription(text)}/>
                { state.event.isSaving &&
                <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                }
                {message && <Text style={styles.errorText}>{message}</Text>}
            </View>
        );
    }

    componentDidMount() {
        log('componentDidMount');
        this._isMounted = true;
        const store = this.props.store;
        this.unsubscribe = store.subscribe(() => {
            log('setState');
            const state = this.state;
            const eventState = store.getState().event;
            this.setState({...state, issue: eventState.issue});
        });
    }

    componentWillUnmount() {
        log('componentWillUnmount');
        this._isMounted = false;
        this.unsubscribe();
        if (this.state.isLoading) {
            this.store.dispatch(cancelSaveEvent());
        }
    }

    updateEventName(name) {
        let newState = {...this.state};
        newState.event.name = name;
        this.setState(newState);
    }

    updateEventDescription(description) {
        let newState = {...this.state};
        newState.event.description = description;
        this.setState(newState);
    }

    onSave() {
        log('onSave');
        this.store.dispatch(saveEvent(this.state.event)).then(() => {
            log('onEventSaved');
            if (!this.state.issue) {
                var routes = this.navigator.state.routeStack;
                for (var i = routes.length - 1; i >= 0; i--) {
                    if (routes[i].name === EventList.routeName) {
                        var destinationRoute = this.props.navigator.getCurrentRoutes()[i];
                        this.navigator.popToRoute(destinationRoute);
                    }
                }
            }
        });
    }
}