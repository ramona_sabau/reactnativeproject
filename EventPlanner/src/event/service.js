import {action, getLogger, errorPayload} from '../core/utils';
import {getAll, save, erase} from './resource';

const log = getLogger('event/service');

const SAVE_EVENT_STARTED = 'event/saveStarted';
const SAVE_EVENT_SUCCEEDED = 'event/saveSucceeded';
const SAVE_EVENT_FAILED = 'event/saveFailed';
const CANCEL_SAVE_EVENT = 'event/cancelSave';

const LOAD_EVENTS_STARTED = 'event/loadStarted';
const LOAD_EVENTS_SUCCEEDED = 'event/loadSucceeded';
const LOAD_EVENTS_FAILED = 'event/loadFailed';
const CANCEL_LOAD_EVENTS = 'event/cancelLoad';

const DELETE_EVENT_STARTED = 'event/deleteStarted';
const DELETE_EVENT_SUCCEEDED = 'event/deleteSucceeded';
const DELETE_EVENT_FAILED = 'event/deleteFailed';
const CANCEL_DELETE_EVENT = 'event/cancelDelete';

export const loadEvents = () => async(dispatch, getState) => {
    log(`loadEvents...`);
    const state = getState();
    const eventState = state.event;
    try {
        dispatch(action(LOAD_EVENTS_STARTED));
        const events = await getAll(state.authentication.token);
        log(`loadEvents succeeded`);
        if (!eventState.isLoadingCancelled) {
            dispatch(action(LOAD_EVENTS_SUCCEEDED, events));
        }
    } catch(err) {
        log(`loadEvents failed`);
        if (!loadEvents.isLoadingCancelled) {
            dispatch(action(LOAD_EVENTS_FAILED, errorPayload(err)));
        }
    }
};

export const cancelLoadEvents = () => action(CANCEL_LOAD_EVENTS);

export const saveEvent = (event) => async(dispatch, getState) => {
    log(`saveEvent...`);
    const state = getState();
    const eventState = state.event;
    try {
        dispatch(action(SAVE_EVENT_STARTED));
        const savedEvent = await save(state.authentication.token, event);
        log(`saveEvent succeeded`);
        if (!eventState.isSavingCancelled) {
            dispatch(action(SAVE_EVENT_SUCCEEDED, savedEvent));
        }
    } catch(err) {
        log(`saveEvent failed`);
        if (!eventState.isSavingCancelled) {
            dispatch(action(SAVE_EVENT_FAILED, errorPayload(err)));
        }
    }
};

export const cancelSaveEvent = () => action(CANCEL_SAVE_EVENT);

export const deleteEvent = (id) => async(dispatch, getState) => {
    log(`deleteEvent...`);
    const state = getState();
    const eventState = state.event;
    try {
        dispatch(action(DELETE_EVENT_STARTED));
        let response = await erase(state.authentication.token, id);
        log(`deleteEvent succeeded`);
        if (!eventState.isSavingCancelled) {
            dispatch(action(DELETE_EVENT_SUCCEEDED));
        }
    } catch(err) {
        log(`deleteEvent failed`);
        if (!eventState.isSavingCancelled) {
            dispatch(action(DELETE_EVENT_FAILED, errorPayload(err)));
        }
    }
};

export const cancelDeleteEvent = () => action(CANCEL_DELETE_EVENT);

export const eventReducer = (state = {items: [], isLoading: false, isSaving: false, isDeleting: false}, action) => { //newState (new object)
    switch (action.type) {
        case LOAD_EVENTS_STARTED:
            return {...state, isLoading: true, isLoadingCancelled: false, issue: null};
        case LOAD_EVENTS_SUCCEEDED:
            return {...state, items: action.payload, isLoading: false};
        case LOAD_EVENTS_FAILED:
            return {...state, issue: action.payload.issue, isLoading: false};
        case CANCEL_LOAD_EVENTS:
            return {...state, isLoading: false, isLoadingCancelled: true};
        case SAVE_EVENT_STARTED:
            return {...state, isSaving: true, isSavingCancelled: false, issue: null};
        case SAVE_EVENT_SUCCEEDED:
            let items = [...state.items];
            let index = items.findIndex((i) => i.id == action.payload.id);
            if (index != -1) {
                items.splice(index, 1, action.payload);
            } else {
                items.push(action.payload);
            }
            return {...state, items, isSaving: false};
        case SAVE_EVENT_FAILED:
            return {...state, issue: action.payload.issue, isSaving: false};
        case CANCEL_SAVE_EVENT:
            return {...state, isSaving: false, isSavingCancelled: true};
        case DELETE_EVENT_STARTED:
            return {...state, isDeleting: true, isDeletingCancelled: false, issue: null};
        case DELETE_EVENT_SUCCEEDED:
            return {...state, isDeleting: false};
        case DELETE_EVENT_FAILED:
            return {...state, issue: action.payload.issue, isDeleting: false};
        case CANCEL_DELETE_EVENT:
            return {...state, isDeleting: false, isDeletingCancelled: true};
        default:
            return state;
    }
};