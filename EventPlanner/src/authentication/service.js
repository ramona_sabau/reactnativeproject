import {action, getLogger, ResourceError, errorPayload} from '../core/utils';
import {getAuthenticatedUser} from './resource';

const log = getLogger('authentication/service');

const AUTHENTICATION_STARTED = 'authentication/started';
const AUTHENTICATION_SUCCEEDED = 'authentication/succeeded';
const AUTHENTICATION_FAILED = 'authentication/failed';

export const login = (user) => async(dispatch, getState) => {
    log(`login`);
    try {
        dispatch(action(AUTHENTICATION_STARTED));
        let authenticatedUser = await getAuthenticatedUser(user);
        log(`log in operation succeeded ` + authenticatedUser);
        dispatch(action(AUTHENTICATION_SUCCEEDED, authenticatedUser));
    } catch (error) {
        log(`log in operation failed`);
        dispatch(action(AUTHENTICATION_FAILED, errorPayload(error)));
    }
};

export const authenticationReducer = (state = {token: null, user: null, isLoading: false}, action) => {
    switch (action.type) {
        case AUTHENTICATION_STARTED:
            return {token: null, user: null, isLoading: true};
        case AUTHENTICATION_SUCCEEDED:
            return {token: action.payload.token, user: action.payload.user, isLoading: false};
        case AUTHENTICATION_FAILED:
            return {issue: action.payload.issue, isLoading: false};
        default:
            return state;
    }
};