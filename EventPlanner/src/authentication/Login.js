import React, {Component} from 'react';
import {Text, View, TextInput, StyleSheet, ActivityIndicator} from 'react-native';
import {login} from './service';
import {getLogger, registerRightAction, issueToText} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('authentication/Login');

const LOGIN_ROUTE = 'authentication/login';

export class Login extends Component {
    static get routeName() {
        return LOGIN_ROUTE;
    }

    static get route() {
        return {
            name: LOGIN_ROUTE,
            title: 'Authentication',
            rightText: 'Login'
        };
    }

    constructor(props) {
        log('constructor');
        super(props);
        this.state = {
            username: '',
            password: ''
        };
        this.store = this.props.store;
        this.navigator = this.props.navigator;
    }

    componentWillMount() {
        log('componentWillMount');
        this.updateState();
        registerRightAction(this.navigator, this.onLogin.bind(this));
    }

    render() {
        log('render');
        const  authentication = this.state.authentication;
        let message = issueToText(authentication.issue);
        return (
            <View style={styles.container}>
                <Text style={styles.detailTitleText}>
                    Username:
                </Text>
                <TextInput onChangeText={(text) => this.setState({...this.state, username: text})}/>
                <Text style={styles.detailTitleText}>
                    Password:
                </Text>
                <TextInput onChangeText={(text) => this.setState({...this.state, password: text})}/>
                { authentication.isLoading &&
                    <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                }
                {message && <Text style={styles.errorText}>{message}</Text>}
            </View>
        )
    }

    componentDidMount() {
        log(`componentDidMount`);
        this.unsubscribe = this.store.subscribe(() => this.updateState());
    }

    componentWillUnmount() {
        log(`componentWillUnmount`);
        this.unsubscribe();
    }

    updateState() {
        log(`updateState`);
        this.setState({...this.state, authentication: this.store.getState().authentication});
    }

    onLogin() {
        log('onLogin');
        this.store.dispatch(login(this.state)).then(() => {
            if (this.state.authentication.token && this.state.authentication.user) {
                this.props.onAuthenticationSucceeded();
            }
        });
    }
}