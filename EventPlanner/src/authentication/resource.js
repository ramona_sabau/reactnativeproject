import {getLogger, ResourceError} from '../core/utils';
import {apiUrl, headers} from '../core/api';

const log = getLogger('authentication/resource');

export async function getAuthenticatedUser(user) {
    log(`getAuthenticatedUser ${apiUrl}/public/login`);
    let ok;
    let json = await fetch(`${apiUrl}/public/login`, {method: 'POST', headers, body: JSON.stringify(user)})
        .then(res => {
            ok = res.ok;
            return res.json();
        });
    if (ok) {
        return json;
    } else {
        throw new ResourceError(`Authentication failed! ` + json.error, json.error);
    }
}