USE EventPlanner;

INSERT INTO user (username, password, first_name, last_name, email) VALUES
  ("user1", "password1", "User1", "User1", "user1@yahoo.com"),
  ("user2", "password2", "User2", "User2", "user2@yahoo.com"),
  ("user3", "password3", "User3", "User3", "user3@yahoo.com"),
  ("user4", "password4", "User4", "User4", "user4@yahoo.com"),
  ("user5", "password5", "User5", "User5", "user5@yahoo.com"),
  ("user6", "password6", "User6", "User6", "user6@yahoo.com"),
  ("user7", "password7", "User7", "User7", "user7@yahoo.com"),
  ("user8", "password8", "User8", "User8", "user8@yahoo.com"),
  ("user9", "password9", "User9", "User9", "user9@yahoo.com"),
  ("user10", "password10", "User10", "User10", "user10@yahoo.com"),
  ("user11", "password11", "User11", "User11", "user11@yahoo.com"),
  ("user12", "password12", "User12", "User12", "user12@yahoo.com"),
  ("user13", "password13", "User13", "User13", "user13@yahoo.com"),
  ("user14", "password14", "User14", "User14", "user14@yahoo.com"),
  ("user15", "password15", "User15", "User15", "user15@yahoo.com"),
  ("user16", "password16", "User16", "User16", "user16@yahoo.com"),
  ("user17", "password17", "User17", "User17", "user17@yahoo.com"),
  ("user18", "password18", "User18", "User18", "user18@yahoo.com"),
  ("user19", "password19", "User19", "User19", "user19@yahoo.com"),
  ("user20", "password20", "User10", "User20", "user20@yahoo.com");

INSERT INTO event (name, description, author_id) VALUES
  ("Event1", "This is event 1. You are all invited!", 1),
  ("Event2", "This is event 2. You are all invited!", 1),
  ("Event3", "This is event 3. You are all invited!", 2),
  ("Event4", "This is event 4. You are all invited!", 3),
  ("Event5", "This is event 5. You are all invited!", 4),
  ("Event6", "This is event 6. You are all invited!", 1),
  ("Event7", "This is event 7. You are all invited!", 1),
  ("Event8", "This is event 8. You are all invited!", 2),
  ("Event9", "This is event 9. You are all invited!",  3),
  ("Event10", "This is event 10. You are all invited!",  4);