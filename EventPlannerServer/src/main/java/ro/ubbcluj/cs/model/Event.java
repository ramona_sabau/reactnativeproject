package ro.ubbcluj.cs.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static javax.persistence.FetchType.EAGER;

@Entity
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @NotNull(message = "Name parameter is requested.")
    @Length(min = 2, max = 50, message = "Name can have at least 2 characters and more than 50.")
    private String name;

    @NotNull(message = "Description parameter is requested.")
    @Length(min = 2, max = 1000, message = "Description can have at least 2 characters and more than 1000.")
    private String description;

    @NotNull(message = "Author of event parameter is requested.")
    @ManyToOne(fetch = EAGER)
    @JoinColumn
    private User author;

    public Event() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
