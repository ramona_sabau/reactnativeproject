package ro.ubbcluj.cs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan(value="ro.ubbcluj.cs.service.security")
public class EventPlannerServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventPlannerServerApplication.class, args);
	}
}
