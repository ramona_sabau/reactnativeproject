package ro.ubbcluj.cs.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubbcluj.cs.controller.dto.AuthenticatedMember;
import ro.ubbcluj.cs.controller.dto.PossibleMember;
import ro.ubbcluj.cs.model.JWTUser;
import ro.ubbcluj.cs.model.User;
import ro.ubbcluj.cs.service.security.JWTService;
import ro.ubbcluj.cs.service.user.UserService;

@RestController
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private JWTService jwtService;

    @PostMapping(value = "/api/public/login")
    public AuthenticatedMember logIn(@RequestBody PossibleMember possibleMember) {
        String username = possibleMember.getUsername();
        String password = possibleMember.getPassword();
        log.info("Log in operation initiated for user with username " + username);

        userService.authenticateUser(username, password);

        JWTUser jwtUser = new JWTUser(username);
        User user = userService.findUserByUsername(username);
        String token = jwtService.getToken(jwtUser);
        return new AuthenticatedMember(token, user);
    }
}
