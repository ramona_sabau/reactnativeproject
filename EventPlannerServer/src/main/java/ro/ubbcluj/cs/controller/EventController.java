package ro.ubbcluj.cs.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubbcluj.cs.model.Event;
import ro.ubbcluj.cs.service.event.EventService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/secured/event")
public class EventController {

    private static final Logger log = LoggerFactory.getLogger(EventController.class);

    private static final String LAST_MODIFIED_HEADER = "last-modified";

    private Date dateOfLastModificationMade = new Date();

    @Autowired
    EventService service;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Event>> getEvents(@RequestHeader(value = LAST_MODIFIED_HEADER) Optional<Date> clientDate,
                                                 HttpServletResponse response) {

        log.info("Get the list with events operation initiated.");
        response.setHeader(LAST_MODIFIED_HEADER, dateOfLastModificationMade.toString());

        Date clientActualDate;
        if (!clientDate.isPresent()) {
            List<Event> events = service.getAllEvents();
            return ResponseEntity.ok(events);
        } else {
            clientActualDate = clientDate.get();
            if (clientActualDate.toString().equalsIgnoreCase(dateOfLastModificationMade.toString())) {
                return ResponseEntity.status(304).body(new ArrayList<>());
            }

            if (clientActualDate.before(dateOfLastModificationMade)) {
                List<Event> events = service.getAllEvents();
                return ResponseEntity.ok(events);
            } else {
                return ResponseEntity.status(304).body(new ArrayList<>());
            }
        }
    }

    @RequestMapping(value = "/{eventName}", method = RequestMethod.GET)
    public Event getEventWithName(@PathVariable("eventName") String eventName) {
            log.info("Get event with the following name = " + eventName + " operation initiated.");
            return service.getEvent(eventName);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Event saveEvent(@Valid @RequestBody Event event) {
        log.info("Save event operation initiated.");
        dateOfLastModificationMade = new Date();
        return service.saveNewEvent(event);
    }

    @RequestMapping(value = "/{eventId}", method = RequestMethod.DELETE)
    public Event deleteEvent(@PathVariable("eventId") long eventId) {
        log.info("Delete event with the following id = " + eventId + " operation initiated.");
        dateOfLastModificationMade = new Date();
        service.deleteEvent(eventId);
        return new Event();
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public Event updateEvent(@Valid @RequestBody Event event) {
        log.info("Update event operation initiated.");
        dateOfLastModificationMade = new Date();
        return service.updateEvent(event);
    }
}
