package ro.ubbcluj.cs.service.event;

import ro.ubbcluj.cs.model.Event;

import java.util.List;

public interface EventService {
    List<Event> getAllEvents();

    Event getEvent(String eventName);

    Event saveNewEvent(Event event);

    Event updateEvent(Event event);

    void deleteEvent(long eventId);
}
