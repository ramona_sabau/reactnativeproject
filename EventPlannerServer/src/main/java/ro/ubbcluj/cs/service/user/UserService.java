package ro.ubbcluj.cs.service.user;

import ro.ubbcluj.cs.model.User;

public interface UserService {

    User findUserByUsername(String username);

    void authenticateUser(String username, String password);
}
