package ro.ubbcluj.cs.service.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubbcluj.cs.model.Event;
import ro.ubbcluj.cs.model.User;
import ro.ubbcluj.cs.repository.EventRepository;
import ro.ubbcluj.cs.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    private static final Logger log = LoggerFactory.getLogger(EventServiceImpl.class);

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @Override
    public Event getEvent(String eventName) {
        Event foundEvent = eventRepository.findOneByName(eventName);

        if (foundEvent == null) {
            String errorMessage = "Event with name = " + eventName + " does not exist.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        return foundEvent;
    }

    @Override
    @Transactional
    public Event saveNewEvent(Event event) {
        User foundUser = checkIfUserExists(event.getAuthor().getId());
        event.setAuthor(foundUser);

        checkIfEventWithNameExists(event.getName());

        log.info(event + " is saved as a new entity.");
        return eventRepository.save(event);
    }

    @Override
    @Transactional
    public Event updateEvent(Event event) {
        Long eventId = event.getId();

        if (eventId == null) {
            String errorMessage = event + " cannot be updated because the identifier is not provided.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        Event foundEvent = checkIfEventWithIdExists(eventId);

        Long authorId = event.getAuthor().getId();
        if (!foundEvent.getAuthor().getId().equals(authorId)) {
            String errorMessage = event + " cannot be updated because the user that made the request is not the author " +
                    "of the event.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        if (foundEvent.getName().equals(event.getName())) {
            foundEvent.setDescription(event.getDescription());

        } else {
            checkIfEventWithNameExists(event.getName());
            foundEvent.setName(event.getName());
            foundEvent.setDescription(event.getDescription());
        }

        log.info(event + " is updated.");
        return eventRepository.save(foundEvent);
    }

    @Override
    @Transactional
    public void deleteEvent(long eventId) {
        Event foundEvent = eventRepository.findOne(eventId);

        if (foundEvent == null) {
            String errorMessage = "Event with id = " + eventId + " does not exist.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        eventRepository.delete(foundEvent);
    }

    private User checkIfUserExists(Long id) {
        User userFound = userRepository.findOne(id);

        if (userFound == null) {
            String errorMessage = "User with id = " + id + " does not exist.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        return userFound;
    }

    private Event checkIfEventWithIdExists(Long id) {
        Event eventFound = eventRepository.findOne(id);

        if (eventFound == null) {
            String errorMessage = "Event with id = " + id + " does not exist.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        return eventFound;
    }

    private void checkIfEventWithNameExists(String name) {
        Event eventFound = eventRepository.findOneByName(name);

        if (eventFound != null) {
            String errorMessage = "Event with name = " + name + " exists.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
