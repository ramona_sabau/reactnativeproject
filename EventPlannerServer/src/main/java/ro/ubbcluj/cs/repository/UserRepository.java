package ro.ubbcluj.cs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.ubbcluj.cs.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findOneByUsername(String username);
}
