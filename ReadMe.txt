Aplicatie Programare pentru dispozitive mobile
---------------------------------------------------------------------------
- As dori sa implementez o aplicatie care permite utilizatorilor sa creeze 
evenimente sportive, culturale etc la care sa poata participa utilizatori 
ce au cont in aplicatie.
- Pentru a putea folosi aplicatia, utilizatorul trebuie sa aiba cont in aplicatie.
- Pentru crearea evenimentului, utilizatorul trebuie sa aleaga numele evenimentului, o locatie, 
o descriere, tipul evenimentului (SPORT, CONFERINTA etc), 
data si ora la care incepe desfasurarea evenimentului. 
- In momentul in care utilizatorii deschid aplicatia, pot vedea o lista cu toate avenimentele la care pot participa.
- Acestia pot participa la un eveniment doar daca apasa pe butonul Join.
- Aplicatia va genera chart-uri cu cate evenimente au fost create de utilizatori,
 cati oameni au participat, cati au apreciat evenimentul si cati nu au fost prea 
incantati.